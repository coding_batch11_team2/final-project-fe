/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import BeforeLoginHeader from './components/BeforeLoginHeader';
import AfterLoginHeader from './components/AfterLoginHeader';
import Footer from './components/Footer';
import Router from './components/Router';
import './assets/Styles.css';

function App() {
  const [IsLoggedIn, setIsLoggedIn] = useState(false);

  const handleLogin = () => {
    // Implement your login logic here and set IsLoggedIn to true if login is successful.
    setIsLoggedIn(true);
  };

  const handleLogout = () => {
    // Implement your logout logic here and set IsLoggedIn to false.
    setIsLoggedIn(false);
  };

  return (
    <>
      {IsLoggedIn ? (
        <AfterLoginHeader onLogout={handleLogout} />
      ) : (
        <BeforeLoginHeader onLogin={handleLogin} />
      )}
      <main>
        <Router IsLoggedIn={IsLoggedIn} onLogin={handleLogin} onLogout={handleLogout} />
      </main>
      <Footer />
    </>
  );
}

export default App;
