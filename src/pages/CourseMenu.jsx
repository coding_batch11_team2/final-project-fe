import * as React from 'react';
    import { styled } from '@mui/material/styles';
    import Box from '@mui/material/Box';
    import {
        createTheme,
        responsiveFontSizes,
        ThemeProvider,
    } from '@mui/material/styles';
    import { Link, NavLink } from 'react-router-dom'; // Import Link from React Router
    import Typography from '@mui/material/Typography';
    import { Button, Card, CardContent, Container, Stack, TextField } from '@mui/material';
    import CardActions from '@mui/material/CardActions';
    import CardMedia from '@mui/material/CardMedia';
    import { useState } from "react";
    import Grid from '@mui/material/Unstable_Grid2';
    import Paper from '@mui/material/Paper';
    import { useParams } from 'react-router-dom';
    import { useEffect } from 'react';
    import axios from 'axios';
    import CardClass from '../components/card/CardClass';
    import bg from '../images/english.png'
    let theme = createTheme();
    theme = responsiveFontSizes(theme);

    const Item = styled(Paper)(({ theme }) => ({
        backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      }));

export default function CourseMenu() {
    const { courseName } = useParams()


    const [isLoading, setIsLoading] = useState(true)
    const [course, setCourse] = useState([])
    const [classes, setClass] = useState([])

    useEffect(() => {
        async function getCourse(name) {
            setIsLoading(true)
            const res = await axios.get(`http://52.237.194.35:2024/api/Type/GetTypeByName?name=${name}`)
            const data = res.data
            setCourse(data)
            setIsLoading(false)
        }

        getCourse(courseName)
    }, [courseName])
    useEffect(() => {
        async function getClasses(name) {
            setIsLoading(true)
            const res = await axios.get(`http://52.237.194.35:2024/api/Menu/GetMenuByTypeName?type_name=${name}`)
            setClass(res.data)
            setIsLoading(false)
        }

        getClasses(courseName)
    }, [courseName])
    
    const prefix = 'data:image/png;base64, ';
    return(
        <>
            <Box marginY={8} sx={{
                // backgroundImage:`url('data:image/png;base64,'+${course.image})`,
                backgroundImage:`url('${prefix}${course.image}')`,
                backgroundRepeat : 'no-repeat',
                backgroundPosition : 'center',
                backgroundSize : 'cover',
                minHeight: '295px',
                width : '100%',
                paddingY : "40px"
            }} >

            </Box>
            
            <Container maxWidth="lg">
            <ThemeProvider theme={theme}>
                    <Typography color="#000000" marginTop={-3} sx={{
                        fontWeight: "bold",
                        fontSize: "24px",
                        fontFamily: "Montserrat"
                    }}>{course.type_name}</Typography>
                    <Typography variant = "body1" marginTop={2} marginBottom={25} color="#333333" sx={{
                    fontSize: "16px"
                    }}>{course.description}</Typography></ThemeProvider>
            </Container>
            <Container maxWidth='lg'>
            {/* <Stack spacing={7} sx={{ alignItems : "center", justifyContent : "center"}}> */}
                {/* <ThemeProvider theme={theme}> */}
                <Typography color="#226957" align='center' sx={{
                    fontWeight: "bold",
                    fontSize: "24px",
                    fontFamily: "Montserrat"
                    }}>Class you might like</Typography>
                {/* </ThemeProvider> */}
                <Grid container spacing={3} marginBottom={5} marginTop={5}>
                    {
                        classes.map((classes, key) =>
                        <Grid item xs={12} md={6} lg={4} key={key}>
                            <Link to={`/courses/${courseName}/${classes.title}`} preventScrollReset={true} style={{textDecoration:"none"}}> 
                            <CardClass classes={classes} type_name={courseName}/>
                            </Link>
                        </Grid>
                        )
                    }
                </Grid>
            {/* </Stack> */}
        </Container>

        </>
    )
}