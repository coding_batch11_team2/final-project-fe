import { Box, Container, Grid, Rating, Stack, Typography, Button,ThemeProvider } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import bg from '../images/class1.png'
import {
    createTheme,
    responsiveFontSizes,
} from '@mui/material/styles';
import CardClass from "../components/card/CardClass";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Style } from "@mui/icons-material";
import { Link } from "react-router-dom";
let theme = createTheme();
theme = responsiveFontSizes(theme);

export default function DetailClass(){
    const {courseName, className} = useParams();
    const [course, setCourse] = useState([])
    const [classes, setClass] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    const [age, setAge] = useState('');

    const handleChange = (event) => {
        setAge(event.target.value);
    };
    //untuk menyimpan variabel kelas yang sedang dipanggil
    let classNow=[];
        //untuk mengambil data course
        // useEffect(() => {
        // async function getCourse(name) {
        //     setIsLoading(true)
        //     const res = await axios.get(`http://52.237.194.35:2024/api/Type/GetTypeByName?name=${name}`)
        //     const data = res.data
        //     setCourse(data)
        //     setIsLoading(false)
        // }

        // getCourse(courseName)
        // }, [courseName])
        //untuk mengambil seluruh kelas dari course yang dipilih
        useEffect(() => {
            async function getClasses(name) {
                setIsLoading(true)
                const res = await axios.get(`http://52.237.194.35:2024/api/Menu/GetMenuByTypeName?type_name=${name}`)
                setClass(res.data)
                setIsLoading(false)
            }

            getClasses(courseName)
        }, [courseName])
        //untuk assign class yang dipanggil saat ini
        classes.map((classs)=>{if (classs.title===className) {
            classNow = classs;
        }})
    return(
        <>
        <Box marginY="100px" paddingX="30px" sx={{fontFamily: "Montserrat"}}> 
        <Grid container spacing={3}>
            <Grid item xs={12} md={4}>
                <img src={"data:image/png;base64, " + classNow.image} width="100%"/>
                
            </Grid>
            <Grid item xs={12} md={8} textAlign="start" >
                <Typography variant="h5" color='#828282' sx={{fontSize:'16px'}} marginBottom={1}>{courseName}</Typography>
                <Typography variant="h4" fontWeight="bold" color="#333333" sx={{fontSize:'24px'}} marginBottom={1}>{classNow.title}</Typography>
                <Typography variant="h4" fontWeight="bold" color='#EA9E1F' sx={{fontSize:'24px'}} marginBottom={1}>IDR {classNow.price}</Typography>
                <Box sx={{ minWidth: 120 }}>
                <FormControl sx={{ m: 1, minWidth: 200, marginLeft: 0, marginTop : 3 }}>
                    <InputLabel id="demo-simple-select-label">Select Schedule</InputLabel>
                    <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={age}
                    label="Select Schedule"
                    onChange={handleChange}
                    >
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                </FormControl>
                </Box>
                <Stack direction='row' spacing={2} marginTop={5.8}>
                <Button
                type="button"
                className="custom-button-reset-yellow"
                variant="contained"
                sx={{ width: "auto", backgroundColor: "#EA9E1F" }} style={{width : '50%'}}
                >
                    <ThemeProvider theme={theme}>
                <Typography color="white" variant="body" sx={{
                  fontFamily: "Montserrat"
                }}>Add to Cart</Typography>
              </ThemeProvider>
                </Button>
                <Button
                type="submit"
                className="custom-button-reset-green"
                variant="contained"
                sx={{ width: "auto", backgroundColor: "#226957" }} style={{width:"50%"}} 
                >
                Buy
                </Button>
                </Stack>
            </Grid>
        </Grid>
        </Box>
        <Container maxWidth="lg">
            <ThemeProvider theme={theme}>
                    <Typography color="#000000" marginTop={-3} sx={{
                        fontWeight: "bold",
                        fontSize: "24px",
                        fontFamily: "Montserrat"
                    }}>Description</Typography>
                    <Typography variant = "body1" marginTop={2} marginBottom={25} color="#333333" sx={{
                    fontSize: "16px"
                    }}>{classNow.description}</Typography></ThemeProvider>
            </Container>
        <Container maxWidth='lg' sx={{marginBottom:"50px"}}>
            <Stack spacing={7} sx={{ alignItems : "center", justifyContent : "center"}}>
                {/* <ThemeProvider theme={theme}> */}
                <Typography color="#226957" sx={{
                    fontWeight: "bold",
                    fontSize: "24px",
                    fontFamily: "Montserrat"
                    }}>Another class for you</Typography>
                {/* </ThemeProvider> */}
                <Grid container spacing={3} >
                    {
                        classes.map((classes, key) =>
                        <Grid item xs={12} md={6} lg={4} key={key}>
                            <Link to={`/courses/${courseName}/${classes.title}`} preventScrollReset={true} style={{textDecoration:"none"}}> 
                            <CardClass classes={classes} type_name={courseName}/>
                            </Link>
                        </Grid>
                        )
                    }
                </Grid>
            </Stack>
        </Container>
        </>
        
    )
}