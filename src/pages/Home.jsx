// Home.js

import React from 'react';
import HeaderAfterLogin from "../components/HeaderAfterLogin"
import Footer from '../components/Footer';
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from '@mui/material/styles';

import '../assets/Styles.css'; // Import your CSS file


let theme = createTheme();
theme = responsiveFontSizes(theme);



function Home() {
  return (
    <div>
        <HeaderAfterLogin />


        <Footer/>


        <div className='FooterBottom'>
            <ThemeProvider theme={theme}>
            </ThemeProvider>
        </div>
    </div>
  );
}

export default Home;
