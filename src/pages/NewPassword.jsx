/* eslint-disable no-unused-vars */
import { Container, TextField, Button, Stack, Grid } from "@mui/material";
import React, { useState } from "react";
import Box from '@mui/material/Box';
import { Link } from 'react-router-dom'; // Import Link from React Router
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import "@fontsource/montserrat";
import '../assets/Styles.css'; // Import your CSS file

let theme = createTheme();
theme = responsiveFontSizes(theme);// untuk Typography responsif

export default function NewPassword() {
  const [pass, setPass] = useState("");
  const [cpass, setCPass] = useState("");

  const handleNewPassword = (e) => {
    setPass(e.target.value);
  };

  const handleConfirmNewPassword = (e) => {
    setCPass(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Proceed with registration logic here
    // Add your registration logic here
    console.log("New Password: ", pass);
    console.log("Confrim New Password: ", cpass);

  };

  return (
    <Container maxWidth="sm">
      <div className='content-wrapper'>
        <Stack spacing={1.5} sx={{ marginBottom: "60px" }}>
              <ThemeProvider theme={theme}>
                <Typography color="black" sx={{
                  fontSize: "24px",
                  fontFamily: "Montserrat"
                }}>Create Password</Typography>
              </ThemeProvider>

  
        </Stack>
            
        <form onSubmit={handleSubmit}>

          <TextField
            label="New Password"
            variant="outlined"
            fullWidth
            margin="normal"
            value={pass}
            onChange={handleNewPassword}
            sx={{ width: "100%" }} // Set the width of the TextField to 100%
            
          />

          <TextField
            label="Confirm New Password"
            variant="outlined"
            fullWidth
            margin="normal"
            value={cpass}
            onChange={handleConfirmNewPassword}
            sx={{ width: "100%" }} // Set the width of the TextField to 100%
            
          />

        <Stack direction="row" spacing={3} justifyContent="flex-end" marginTop={3}>
          <Link to="/login">
            <Button
              type="button"
              className="custom-button-reset-yellow"
              variant="contained"
              sx={{ width: "auto", backgroundColor: "#EA9E1F" }}
            >
              Cancel
            </Button>
          </Link>
          <Link to="/login">
            <Button
              type="submit"
              className="custom-button-reset-green"
              variant="contained"
              sx={{ width: "auto", backgroundColor: "#226957" }}
            >
              Submit
            </Button>
          </Link>
        </Stack>
        </form>
      </div>
    </Container>
  );
}
