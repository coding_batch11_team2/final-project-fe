import { Container, TextField, Button, Stack } from "@mui/material";
import { useState } from "react";
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from "@mui/material/styles";
import "@fontsource/montserrat";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";
import "../assets/Styles.css";

let theme = createTheme();
theme = responsiveFontSizes(theme);

export default function Register() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleConfirmPasswordChange = (e) => {
    setConfirmPassword(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Proceed with registration logic here
    // Add your registration logic here
    console.log("Name:", name);
    console.log("Email:", email);
    console.log("Password:", password);
    console.log("Confirm Password:", confirmPassword);
  };

  return (
    <Container maxWidth="sm" className="ContainerMargin">
      <div className="content-wrapper">
        <Stack spacing={1}>
          <ThemeProvider theme={theme}>
            <Stack
              direction="row"
              spacing={1}
              // Center the text horizontally
              alignItems="center" // Center the text vertically
              sx={{
                fontWeight: "bold",
                fontSize: "24px",
                fontFamily: "Montserrat",
                color: "#226957",
              }}
            >
              <Typography sx={{
                fontWeight: "bold",
                fontSize: "24px",
                fontFamily: "Montserrat",
                color: "#226957",
              }}>Let&apos;s join</Typography>
              <Typography color="#ea9e1f" sx={{ fontSize: "36px" }}>
                D&apos;Language
              </Typography>
            </Stack>
          </ThemeProvider>
        </Stack>
        {/*&apos; ini adalah tanda PETIK */}
        <Typography marginTop={3} marginBottom={3} sx={{
          fontSize: "16px"
        }}>Please register here</Typography>
        
        <form onSubmit={handleSubmit}>
          <TextField
            label="Name"
            variant="outlined"
            fullWidth
            margin="normal"
            value={name}
            onChange={handleNameChange}
          />
          <TextField
            label="Email"
            variant="outlined"
            fullWidth
            margin="normal"
            value={email}
            onChange={handleEmailChange}
          />
          <TextField
            label="Password"
            variant="outlined"
            type="password"
            fullWidth
            margin="normal"
            value={password}
            onChange={handlePasswordChange}
          />
          <TextField
            label="Confirm Password"
            variant="outlined"
            type="password"
            fullWidth
            margin="normal"
            value={confirmPassword}
            onChange={handleConfirmPasswordChange}
          />
        
        <Stack marginTop={3} justifyContent="flex-end" alignItems="flex-end">
          <Link to = "/emailconfirmation">
          <Button
            type="submit"
            className="custom-button-green custom-button"
            variant="contained"
            sx={{ backgroundColor: "#226957", width: "auto" }}
            >
            Sign Up
          </Button>
          </Link>
        </Stack>
        
        <Stack marginTop={5} justifyContent="center" gap={0.5} direction='row'>
          <ThemeProvider theme={theme}>
            <Typography sx={{
              fontSize: "16px"
            }}>Have an account?</Typography>
            <Link to="/login">
              <Typography sx={{
                fontSize: "16px",
                color: "blue"
              }}>Login here</Typography>
            </Link>
          </ThemeProvider>
        </Stack>
        </form>
      </div>
    </Container>
  );
}
