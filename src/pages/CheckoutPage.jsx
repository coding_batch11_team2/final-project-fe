/* eslint-disable no-unused-vars */
import React from 'react';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Container, Divider } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import useGetClass from '../hooks/useGetClass';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import "@fontsource/montserrat";

import '../assets/Styles.css'; // Import your CSS file

const CheckoutPage = () => {
    const {loading1, classes} = useGetClass()

  return (
    <>
    <Container maxWidth="xl" className='marginTopCheckout' spacing={2} sx={{
        fontFamily: "Montserrat"
        }}>
      {/* Checkout form or class selection */}
      <Grid item md={12} className='marginTopCheckout'>
        <Paper sx={{ padding: 2 }}>
          <Grid container spacing={2}>
            {/* Item List */}
            {classes.map((classes, key) => (
              <Grid item xs={12} md={12} container alignItems="center" key={key}>
                <Grid item xs={2} md={1}>
                  <Checkbox />
                </Grid>
                <Grid item xs={4} md={2} >
                  {/* Item Picture */}
                  <img className='imgSize' src={"data:image/png;base64, " + classes.image} alt={classes.name} />
                    
                </Grid>
                <Grid item xs={12} md={8}>
                  {/* Item Name */}
                  <Typography gutterBottom color='#828282' variant="body2" component="div" sx={{fontSize:'16px'}}>
                    {classes.type_name}
                </Typography>
                <Typography variant="h6" fontWeight='bold' marginTop={-1} color="#333333" sx={{fontSize:'18px'}}>
                    {classes.title}
                </Typography>
                <Typography>
                    Schedule, Wednesday, 27 July 2022.
                </Typography>
                <Typography variant= 'h4' color='#ea9e1f' fontWeight='bold' marginBottom={1} marginTop={1} 
             sx={{fontSize:'20px'}}>IDR {classes.price}</Typography>
                </Grid>
                <Grid item xs={1} md={1}>
                  {/* Delete Button */}
                  <DeleteForeverIcon  sx={{color:"red"}}  />
                </Grid>
              </Grid>
            ))}
          </Grid>
        </Paper>
      </Grid>
      <Divider />
      <Grid item xs={12} md={12}>
        <Paper sx={{ padding: 5 }}>
        <Grid container spacing={6} justifyContent="center">
            {/* Payment Summary */}
            <Grid item >
            <Typography variant="h6">Total Price</Typography>
            {/* Add your payment summary details here */}
            </Grid>
            <Grid item xs={8} >
            <Typography className='TotalPrice' variant="h6">IDR 700.000</Typography>
            </Grid>
            {/* Pay Button */}
            <Grid item xs={2}>
            <Button sx={{backgroundColor:"#226957"}} variant="contained" color="primary" fullWidth>
                Pay Now
            </Button>
            </Grid>
        </Grid>
        </Paper>
     </Grid>
     </Container>

    </>
  );
};

export default CheckoutPage;
