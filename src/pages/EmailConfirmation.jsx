/* eslint-disable no-unused-vars */
import { Container, TextField, Button, Stack, Grid } from "@mui/material";
import React, { useState } from "react";
import Box from '@mui/material/Box';
import { Link } from 'react-router-dom'; // Import Link from React Router
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from '@mui/material/styles';
import Avatar from '@mui/material/Avatar';
import iconImage from '../images/emailconfir.svg';
import Typography from '@mui/material/Typography';
import "@fontsource/montserrat";
import '../assets/Styles.css'; // Import your CSS file

let theme = createTheme();
theme = responsiveFontSizes(theme);// untuk Typography responsif

export default function NewPassword() {
  return (
    <Container maxWidth="sm">
      <div className='content-wrapper'>
        <Stack spacing={5} sx={{ alignItems : "center", justifyContent : "center"}}>
            <Stack sx = {{marginBottom :  "60px"}}>
                <Avatar src={iconImage} alt="Logo" sx={{ width : 225.49, height: 243 }} />
            </Stack>
            <Stack spacing={0.5} sx = {{alignItems : "center", justifyContent : "center"}}> 
            <ThemeProvider theme={theme}>
            <Typography color="#226957" sx={{
                    fontWeight:"bold",
                    fontSize : "24px"
                    //fontFamily : "Montserrat"
                    }}>Email Confirmation Success</Typography>
                <Typography color="#4F4F4F" sx={{
                    fontSize : "16px",
                    //fontFamily : "Montserrat"
                    }}>Thanks for confirmation your email. Please login first
                </Typography>
                </ThemeProvider>
            </Stack>
            <Link to = "/login">
                <Button
                type="submit"
                className="custom-button-reset-green"
                variant="contained"
                sx={{ width: "auto", backgroundColor: "#226957" }}
                >
                Login Here
            </Button>
            </Link>
        </Stack>
      </div>
    </Container>
  );
}
