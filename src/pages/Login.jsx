/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from '@mui/material/styles';
import { Link, useNavigate } from 'react-router-dom'; // Import Link and useHistory
import Typography from '@mui/material/Typography';
import { Button, Container, Stack, TextField } from '@mui/material';
import PropTypes from 'prop-types';

let theme = createTheme();
theme = responsiveFontSizes(theme);

const Login = ({ onLogin }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate(); // Use useNavigate to navigate

  const handleClickLogin = () => {
    console.log('Login button clicked'); // Add this for debugging

    // Validate the email and password here, and call onLogin if credentials are correct
    if (email === 'user@example.com' && password === 'password') {
        onLogin();
        navigate('/'); // Redirect to the landing page
    } else {
      // Handle incorrect credentials, show an error message, etc.
      window.alert('Incorrect email or password. Please try again.');

    }
  };

  return (
    <Container maxWidth="sm" className="ContainerMargin" sx={{ fontFamily: 'Montserrat' }}>
      <div className="content-wrapper">
        <ThemeProvider theme={theme}>
          
          <Typography color="#226957" sx={{ fontWeight: 'bold', fontSize: '24px', fontFamily: 'Montserrat' }}>
            Welcome Back!
          </Typography>
          <Typography marginTop={3} marginBottom={3} sx={{ fontSize: '16px' }}>
            Please login first
          </Typography>
        </ThemeProvider>

        <form>
          <TextField
            label="Email"
            variant="outlined"
            fullWidth
            margin="normal"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            label="Password"
            variant="outlined"
            type="password"
            fullWidth
            margin="normal"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Stack marginTop={2} gap={0.5} direction="row">
            <Typography sx={{ fontSize: '16px' }}>Forgot Password? </Typography>
            <Link to="/resetpassword">
              <Typography sx={{ fontSize: '16px', color: 'blue' }}>Click Here</Typography>
            </Link>
          </Stack>

          <Stack marginTop={3} justifyContent="flex-end" alignItems="flex-end">
            <Button
              type="button"
              className="custom-button-green custom-button"
              variant="contained"
              sx={{ backgroundColor: '#226957', width: 'auto' }}
              onClick={handleClickLogin}
            >
              Login
            </Button>
          </Stack>

          <Stack marginTop={5} justifyContent="center" gap={0.5} direction="row">
            <Typography sx={{ fontSize: '16px' }}>Don&apos;t have an account?</Typography>
            <Link to="/register">
              <Typography sx={{ fontSize: '16px', color: 'blue' }}>Sign Up here</Typography>
            </Link>
          </Stack>
        </form>
      </div>
    </Container>
  );
};

Login.propTypes = {
  onLogin: PropTypes.func.isRequired,
};

export default Login;
