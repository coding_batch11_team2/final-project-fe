/* eslint-disable no-unused-vars */
import React from 'react';
import PicBox from '../components/banner/PicBox'
import PicCard from '../components/banner/PicSection'
import PicBoxYellow from '../components/banner/PicBoxYellow';
import PicCardFlag from '../components/banner/PicCardFlag';
import PicCardProduct from '../components/banner/PicCardProduct';
import {
    createTheme,
    responsiveFontSizes,
} from '@mui/material/styles';

import '../assets/Styles.css'; // Import your CSS file
import useGetCourse from '../hooks/useGetCourse';
import useGetClass from '../hooks/useGetClass';

let theme = createTheme();
theme = responsiveFontSizes(theme);

const LandingPage = () => {
    const {loading, courses} = useGetCourse()
    const {loading1, classes} = useGetClass()

    return (
        
        <div>
            {/*// First Section: Container with Images */}
            <PicBox />
        
            {/* Second Section: 3-Row Grid with Text */}
            <PicCard />

            {/* Third Section: 3-Row Grid with Text */}
             <PicCardProduct />         
            
            {/* Fourth Section: 3-Row Grid with Text */}
            <PicBoxYellow />

            {/* Fifth Section: 3-Row Grid with Text */}
             <PicCardFlag /> 
            
        {/* <div className='FooterBottom' >
            <ThemeProvider theme={theme}>
            </ThemeProvider>
        </div> */}
        </div>
    );
}

export default LandingPage;
