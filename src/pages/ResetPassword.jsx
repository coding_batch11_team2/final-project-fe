/* eslint-disable no-unused-vars */
import { Container, TextField, Button, Stack, Grid } from "@mui/material";
import React, { useState } from "react";
import Box from '@mui/material/Box';
import { Link } from 'react-router-dom'; // Import Link from React Router
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import "@fontsource/montserrat";
import '../assets/Styles.css'; // Import your CSS file

let theme = createTheme();
theme = responsiveFontSizes(theme);// untuk Typography responsif

export default function ResetPassword() {
  const [email, setEmail] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Proceed with registration logic here
    // Add your registration logic here
    console.log(email);
  };

  return (
    <Container maxWidth="sm">
      <div className='content-wrapper'>
        <Stack spacing={1.5} sx={{ marginBottom: "60px" }}>
          <ThemeProvider theme={theme}>
            <Stack direction="row" spacing={1.5}>
              <ThemeProvider theme={theme}>
                <Typography color="black" sx={{
                  fontSize: "24px",
                  fontFamily: "Montserrat"
                }}>Reset Password</Typography>
              </ThemeProvider>
            </Stack>

            <Typography sx={{
              fontSize: "16px",
              fontFamily: "Montserrat"
            }}>Please enter your email address</Typography>
          </ThemeProvider>
        </Stack>
            
        <form onSubmit={handleSubmit}>

          <TextField
            label="Email"
            variant="outlined"
            fullWidth
            margin="normal"
            value={email}
            onChange={handleEmailChange}
            sx={{ width: "100%" }} // Set the width of the TextField to 100%

          />

        <Stack direction="row" spacing={3} justifyContent="flex-end" marginTop={3}>
          <Link to="/login">
            <Button
              type="button"
              className="custom-button-reset-yellow"
              variant="contained"
              sx={{ width: "auto", backgroundColor: "#EA9E1F" }}
            >
              Cancel
            </Button>
          </Link>
          <Link to="/newpassword">
          <Button
            type="submit"
            className="custom-button-reset-green"
            variant="contained"
            sx={{ width: "auto", backgroundColor: "#226957" }}
          >
            Confirm
          </Button>
          </Link>
        </Stack>
        </form>
      </div>
    </Container>
  );
}
