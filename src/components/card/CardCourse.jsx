/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';
import { Rating } from '@mui/material';

export default function CardCourse({ course }) {
    return (
        <Card sx={{minHeight: 200}}>
            <CardMedia
                sx={{ height: 140 }}
                image={"data:image/png;base64, " + course.image}
                title={course.type_name}
            />
            <CardContent>
                {/* <Rating value={product.rating} readOnly/> */}
                <Link to={`/courses/${course.type_name}`} preventScrollReset={true} style={{textDecoration:"none"}}> 
                <Typography variant="h6" color="black" sx={{fontSize:'24px'}} fontWeight='400' fontFamily = "Montserrat" marginBottom={-2}> 
                    {course.type_name}
                </Typography>
                {/* <Button size="small">{course.type_name}</Button> */}
                </Link>
                               
                {/* <Link to={`/courses/${course.type_name}`} preventScrollReset={true}> 
                <Typography variant="h6" color="text.secondary">
                    {course.title}
                </Typography>
                </Link> */}
                
            </CardContent>
            {/* <CardActions>
                <Link to={`/courses/${course.type_name}`} preventScrollReset={true}> 
                    <Typography >{course.price}</Typography>
                </Link>
            </CardActions> */}
        </Card>
    );
}