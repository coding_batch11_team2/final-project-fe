/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';
import { Rating } from '@mui/material';

export default function CardClass({classes, type_name}) {
    return (
        <Card sx={{minHeight: 200, fontFamily: "Montserrat"}} >
            <CardMedia
                sx={{ height: 140 }}
                image={"data:image/png;base64, " + classes.image}
                title={classes.type_name}
            />
            <CardContent>
                {/* <Rating value={product.rating} readOnly/> */}
                <Typography gutterBottom color='#828282' variant="body2" component="div" sx={{fontSize:'16px'}}>
                    {type_name}
                </Typography>
                <Typography variant="h6" fontWeight='bold' marginTop={-1} color="#333333" sx={{fontSize:'18px'}}>
                    {classes.title}
                </Typography>
                    
            </CardContent>
            <CardActions>
            <Typography variant= 'h4' color='#226957' fontWeight='bold' marginBottom={0} 
            marginLeft={1} sx={{fontSize:'20px'}}>IDR {classes.price}</Typography>
            </CardActions>
        </Card>
    );
}