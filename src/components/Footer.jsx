import { Grid, Container, Typography, Stack} from '@mui/material';
import EmailIcon from '../images/icon/email.png'
import PhoneIcon from '../images/icon/phone.png'
import InstagramIcon from '../images/icon/instagram.png'
import TelegramIcon from '../images/icon/telegram-logo.png'
import YoutubeIcon from '../images/icon/youtube.png'
import "@fontsource/montserrat";
import { Link } from 'react-router-dom';
import useGetCourse from '../hooks/useGetCourse';

const Footer = () => {
  const {loading, courses} = useGetCourse()
  return (
    <Container maxWidth={false} className='footerCSS' sx={{
      fontFamily: "Montserrat"
      }}>
      <Grid container spacing={3}>
      
        <Grid item xs={12} sm={6} md={4}>
          <Typography variant="h6">About Us</Typography>
          <Typography>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Typography variant="h6" >Product</Typography>
          <Container>
          <Grid container textAlign="justify" >
          {
              courses.map((course, key) =>
                  <Grid item xs={6} key={key}>
                      <li ><Link to={`/courses/${course.type_name}`} style={{textDecoration:"none"}} color='black'> 
                        <Typography variant='body' color="white">{course.type_name}</Typography></Link>
                      </li>
                  </Grid>
                  
              )
          }
            {/* <Grid xs={6}><li>Arabic</li></Grid>
            <Grid xs={6}><li>English</li></Grid>
            <Grid xs={6}><li>Indonesian</li></Grid>
            <Grid xs={6}><li>Mandarin</li></Grid>
            <Grid xs={6}><li>Deutsch</li></Grid>
            <Grid xs={6}><li>French</li></Grid>
            <Grid xs={6}><li>Japanese</li></Grid>
            <Grid xs={6}><li>Melayu</li></Grid> */}
          </Grid>
          </Container>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
            <Grid>
                <Typography variant="h6">Address</Typography>
                <Typography>Sed ut perspiciatis unde omnis iste natus error sit 
                            voluptatem accusantium doloremque.
                </Typography>
            </Grid>
            <Grid>
                <Typography variant="h6" marginTop={1}>Contact Us</Typography> 
                <Stack direction='row' spacing={1} marginTop={1}>
                    <div className="round-icon">
                      <img src={PhoneIcon} alt="Phone Icon" />
                    </div>
                    <div className="round-icon">
                      <img src={InstagramIcon} alt="Instagram Icon" />
                    </div>
                    <div className="round-icon">
                      <img src={YoutubeIcon} alt="Youtube Icon" />
                    </div>
                    <div className="round-icon">
                      <img src={TelegramIcon} alt="Telegram Icon" />
                    </div>
                    <div className="round-icon">
                      <img src={EmailIcon} alt="Email Icon" />
                    </div>
                </Stack>
         
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Footer;
