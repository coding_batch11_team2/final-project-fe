/* eslint-disable no-unused-vars */
import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import iconImage from '../images/logo.png';
import { Link, useNavigate } from 'react-router-dom';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { Box, Divider, Drawer, List, ListItem, ListItemIcon, ListItemText, ListItemButton } from '@mui/material';
import "@fontsource/montserrat";
import PropTypes from 'prop-types';
import PersonIcon from '@mui/icons-material/Person';


const AfterLoginHeader = ({ onLogout }) => {
  const navigate = useNavigate();

  const [drawerOpen, setDrawerOpen] = React.useState({
    right: false,
  });

  const handleClickLogout = () => {
    onLogout();
    navigate('/');
  };

    const toggleDrawer = (anchor, open) => (event) => {
      if (
        event &&
        event.type === 'keydown' &&
        (event.key === 'Tab' || event.key === 'Shift')
      ) {
        return;
      }
  
      setDrawerOpen({ ...drawerOpen, [anchor]: open });
    };

    const list = (anchor) => (
      <Box
        sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
        role="presentation"
        onClick={toggleDrawer(anchor, false)}
        onKeyDown={toggleDrawer(anchor, false)}
      >
        <List>
          {['Cart', 'My Class', 'Invoice', 'Account', 'LogOut'].map((text, index) => (
            <ListItem key={text} disablePadding>
              <ListItemButton>
                <ListItemIcon>
                 
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
        <Divider />
      </Box>
    );

  return (
    <header
      style={{
        fontFamily: 'Montserrat',
        marginBottom: '20px',
      }}
    >
      <AppBar className="full-width-app-bar" elevation={0}>
        <Toolbar item="true">
          <Link to="/" className="link-no-underline">
            <Avatar src={iconImage} alt="Logo" sx={{ marginRight: '10px' }} />
          </Link>

          <Typography color="black" variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Link to="/" className="link-no-underline">
              Language
            </Link>
          </Typography>

          <Link to="/">
            <Button startIcon={<ShoppingCartIcon />} className="buttonAppBar">
            </Button>
          </Link>
          <Link to="/">
            <Button className="buttonAppBar">My Class</Button>
          </Link>
          <Link to="/">
            <Button className="buttonAppBar">Invoice</Button>
          </Link>
          <Typography color="black">
            |
          </Typography>
            <PersonIcon className="buttonAppBarAccount"/>
          <Link to="/">
            <Button
              endIcon={<ExitToAppIcon />}
              className="buttonLogout"
              onClick={handleClickLogout}
            >
            </Button>
          </Link>
        </Toolbar>
      </AppBar>
    </header>
  );
};  

AfterLoginHeader.propTypes = {
  onLogout: PropTypes.func.isRequired,
};

export default AfterLoginHeader;
