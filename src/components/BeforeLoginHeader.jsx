/* eslint-disable react/no-unknown-property */
// eslint-disable-next-line no-unused-vars
import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import iconImage from '../images/logo.png';
import { Link } from 'react-router-dom'; // Import Link from React Router
import "@fontsource/montserrat";

import '../assets/Styles.css'; // Import your CSS file

const Header = () => (
  <header sx={{
    fontFamily: "Montserrat",
    marginBottom: "20px"
    }}>
    <AppBar className="full-width-app-bar" elevation={0}>
      <Toolbar item="true">
        <Link to="/" className='link-no-underline'>
            <Avatar src={iconImage} alt="Logo" sx={{ marginRight: '10px' }} />
            </Link>
            
            <Typography color='black' variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Link to="/" className='link-no-underline'>
              Language
            </Link>
            </Typography>
        
            
            <Link to="/login">
              <Button className="custom-button-green">Login</Button>
            </Link>
            <Link to="/register">
              <Button className="custom-button-yellow">Sign Up</Button>
            </Link>
       </Toolbar> 
    </AppBar>
  </header>
);

export default Header;
