/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import PropTypes from 'prop-types'; // Import PropTypes
import Register from '../pages/Register';
import Login from '../pages/Login';
import ResetPassword from '../pages/ResetPassword';
import NewPassword from '../pages/NewPassword';
import EmailConfirmation from '../pages/EmailConfirmation'
import LandingPage from "../pages/LandingPage";
import CourseMenu from '../pages/CourseMenu';
import DetailClass from '../pages/DetailClass';
import CheckoutPage from "../pages/CheckoutPage";


{/*API
https://dummyjson.com/products
*/}

export default function Router({  onLogin }) {
  
  return (
    
    <Routes>
         
      <Route path="/" element={<LandingPage />} />
      <Route path="/register" element={<Register />} />
      <Route path="/login" element={ <Login onLogin={onLogin}/>} />
      <Route path="/resetpassword" element={<ResetPassword />} />
      <Route path="/newpassword" element={<NewPassword />} />
      <Route path="/emailconfirmation" element={<EmailConfirmation />} />
      <Route path="/coursemenu" element={<CourseMenu />} />
      <Route path="/courses/:courseName/:className" element={<DetailClass />} />
      <Route path="/courses/:courseName" element={<CourseMenu/>} />
      <Route path="/checkout" element={<CheckoutPage/>} />



    </Routes>
  )
  }

  Router.propTypes = {
    IsLoggedIn: PropTypes.bool.isRequired,
    onLogin: PropTypes.func.isRequired,
  };