/* eslint-disable no-unused-vars */
import React from 'react';
import { Typography, Box, Grid, Container } from "@mui/material";
import Model1 from "../../images/Model.png";
import "@fontsource/montserrat";
import '../../assets/Styles.css'; // Import your CSS file

const PicBoxYellow = () => {
  return (
    <Box className="yellowPicBox">
      <Container>
        <Grid container spacing={2} justifyContent="center">
          <Grid item xs={12} md={8} className="centered-text">
            <div>
              <Typography variant="h4" sx={{ fontFamily: "Montserrat" }}>
                Get Your Best Benefits
              </Typography>
              <Typography textAlign="justify" sx={{ fontFamily: "Montserrat" }}>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae vitae
                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
                eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est,
                qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
                sed quia non numquam eius modi tempora incidunt ut labore et dolore
                magnam aliquam quaerat voluptatem.
              </Typography>
            </div>
          </Grid>
          <Grid item xs={12} md={4} className="centered-image">
            <div className="centered-image">
              <img
                src={Model1}
                alt="Image 1"
                className="front-image"
              />
            </div>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default PicBoxYellow;
