import { Container, Grid, Typography } from '@mui/material';
import "@fontsource/montserrat";

const PicSection = () => {


return(
<Container maxWidth="md" align="center" className='MarginTB' fontSize="16px" sx={{
                fontFamily: "Montserrat"
                }}>
                
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6} md={4}>
                        <Typography variant="h3" color="#226957">100+</Typography>
                        <p>Choose the class you like and get the skills</p>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                        <Typography variant="h3" color="#226957">50+</Typography>
                        <p>Having teachers who are highly skilled and competent in the language</p>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                        <Typography variant="h3" color="#226957">10+</Typography>
                        <p>Many alumni become ministry employees because of their excellent language skills</p>
                    </Grid>
                </Grid>
            </Container>
)
}

export default PicSection;