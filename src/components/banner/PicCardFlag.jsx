/* eslint-disable no-unused-vars */
import { Container, Grid, Typography, Card, CardMedia} from '@mui/material';
import { Link } from 'react-router-dom'; // Import Link component
import "@fontsource/montserrat";
import CardCourse from '../card/CardCourse';
import useGetCourse from '../../hooks/useGetCourse';
import { useEffect } from 'react';
import '../../assets/Styles.css';

const PicCardFlag = () => {
  const {loading, courses} = useGetCourse()


  return (
    <Container align="center" className='MarginTB' sx={{
      fontFamily: "Montserrat"
      }}>
          <Typography variant="h4" marginBottom={10}>
              Available Language Courses
          </Typography>
          <Grid container spacing={6}>
          {
              courses.map((course, key) =>
                  <Grid item xs={12} md={4} lg={3} key={key}>
                    <Link to={`/courses/${course.type_name}`} className='link-no-underline'> 
                      <CardCourse course={course}/>
                    </Link>
                  </Grid>
                  
              )
          }
          </Grid>
    </Container>
  );
};

export default PicCardFlag;
