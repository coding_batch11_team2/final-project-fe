/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-unused-vars
import React from 'react';
import { Container, Typography, Grid, Card, CardContent, CardMedia } from '@mui/material';
// eslint-disable-next-line no-unused-vars
import { ThemeProvider } from '@mui/system';
import useGetClass from '../../hooks/useGetClass';
import CardClass from '../card/CardClass';
import { Link } from 'react-router-dom';
import "@fontsource/montserrat";

import '../../assets/Styles.css'; // Import your CSS file

const PicCardProduct = () => {
  const {loading1, classes} = useGetClass()

  return (
      <Container maxWidth="lg" className='MarginTB' sx={{
      fontFamily: "Montserrat"
      }}>
                <Typography variant="h4" marginBottom={10} textAlign='center'>
                    Recommended Class
                </Typography>
                <Grid container spacing={6} >
                {
                    classes.map((classes, key) =>
                        <Grid item xs={12} md={6} lg={4} key={key}>
                            <Link to={`/courses/${classes.type_name}/${classes.title}`} preventScrollReset={true} style={{textDecoration:"none"}}> 
                            <CardClass classes={classes} type_name={classes.type_name}/>
                            </Link>
                        </Grid>
                    )
                }
                </Grid>
            </Container>
  );
};

export default PicCardProduct;
