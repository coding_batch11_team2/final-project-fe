import { Typography, Box, Container } from '@mui/material';
import "@fontsource/montserrat";

const PicBox = () => {
  return (
    <Box className="PicBox">
      <Container>  
        <Typography variant="h4" className="headline" xs={12} sm={6} md={4}sx={{
                fontFamily: "Montserrat"
                }}>
          Learn different languages to hone your communication skills
        </Typography>
        <Typography className="description" item xs={12} sm={6} md={4} sx={{
                fontFamily: "Montserrat"
                }}>
          All the languages you are looking for are available here, so what are you waiting for and immediately improve your language skills
        </Typography>
      </Container>
    </Box>
  );
};

export default PicBox;
