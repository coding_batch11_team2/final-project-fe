/* eslint-disable no-unused-vars */
import { Typography } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";

export default function useGetClass() {
    const [loading, setLoading] = useState(false)
    const [classes, setClasses] = useState([])

    useEffect(() => {
        async function getClasses() {
            setLoading(true)
            const res = await axios.get('http://52.237.194.35:2024/api/Menu/GetMenuLimit')
            setClasses(res.data)
            setLoading(false)
        }

        getClasses()
    }, [])

    return {
        loading,
        classes
    }
}

/*Buat ngambil data dari API dan buat card lading */
