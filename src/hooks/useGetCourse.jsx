/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { Typography } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";

export default function useGetCourse() {
    const [loading, setLoading] = useState(false)
    const [courses, setCourses] = useState([])

    useEffect(() => {
        async function getCourses() {
            setLoading(true)
            const res = await axios.get('http://52.237.194.35:2024/api/Type/GetActiveType')
            setCourses(res.data)
            setLoading(false)
        }

        getCourses()
    }, [])

    return {
        loading,
        courses
    }
}