"use client";
import {
  Avatar_default,
  avatarClasses_default,
  getAvatarUtilityClass
} from "./chunk-JOKN4OE5.js";
import "./chunk-QOTPRLAI.js";
import "./chunk-2HBRCMZH.js";
import "./chunk-A5Z43GNQ.js";
import "./chunk-BHZAVQOK.js";
export {
  avatarClasses_default as avatarClasses,
  Avatar_default as default,
  getAvatarUtilityClass
};
//# sourceMappingURL=@mui_material_Avatar.js.map
