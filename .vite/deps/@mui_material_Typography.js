"use client";
import {
  Typography_default,
  getTypographyUtilityClass,
  typographyClasses_default
} from "./chunk-4P4MEQCQ.js";
import "./chunk-QOTPRLAI.js";
import "./chunk-2HBRCMZH.js";
import "./chunk-A5Z43GNQ.js";
import "./chunk-BHZAVQOK.js";
export {
  Typography_default as default,
  getTypographyUtilityClass,
  typographyClasses_default as typographyClasses
};
//# sourceMappingURL=@mui_material_Typography.js.map
