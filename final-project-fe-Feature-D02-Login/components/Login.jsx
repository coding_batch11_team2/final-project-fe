import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import {
    createTheme,
    responsiveFontSizes,
    ThemeProvider,
  } from '@mui/material/styles';
  import Typography from '@mui/material/Typography';
import { Button, Card, CardContent, Container, Link, Stack, TextField } from '@mui/material';
  
let theme = createTheme();
theme = responsiveFontSizes(theme);

const Login = () => {
    return(
        <>
            <Box marginTop={5}>
                <Container maxWidth='sm'>
                    <Stack spacing={1.5}>
                        <ThemeProvider theme={theme}>
                        <Typography color="#226957" sx={{
                            fontWeight:"bold",
                            fontSize : "24px"
                            //fontFamily : "Montserrat"
                        }}>Welcome Back!</Typography>
                        <Typography sx={{
                            fontSize : "16px"
                        }}>Please login first.</Typography>
                        </ThemeProvider>
                    </Stack>
                    <Stack marginTop={3} spacing={2}> 
                    <TextField id="outlined-basic" label="Email" variant="outlined" required/>
                    <TextField id="outlined-basic" label="Password" variant="outlined" required/>
                    </Stack>
                    <Stack direction = 'row' spacing={1} marginTop={1}>
                    <ThemeProvider theme={theme}>
                    <Typography sx={{
                            fontSize : "16px"
                        }} >Forgot Password?</Typography>
                    <Typography sx={{
                            fontSize : "16px"
                        }} color='blue'><Link to='/'></Link> Click Here</Typography>
                    </ThemeProvider>
                    </Stack>
                    <Stack marginTop={1}>
                        <Box component='div' textAlign='end'>
                            <Button variant="contained" sx={{
                                backgroundColor:"#226957"
                            }}>Login</Button>
                        </Box>
                    </Stack>
                    <Stack marginTop={5} justifyContent="center" gap={0.5} direction='row'>
                    <ThemeProvider theme={theme}>
                    <Typography sx={{
                            fontSize : "16px"
                        }}>Don't have account?</Typography>
                    <Typography sx={{
                            fontSize : "16px"
                        }}color="blue">Sign Up here</Typography>
                    </ThemeProvider>
                    </Stack>
                </Container>
            </Box>
        </>
    )
}
export default Login;