import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import iconImage from '../images/logo.png';
import '../assets/Styles.css'; // Import your CSS file

const Header = () => (
  <header>
    <AppBar className="full-width-app-bar" elevation={0}>
      <Toolbar >
      <Avatar src={iconImage} alt="Logo" sx={{ marginRight: '10px' }} />
        <Typography color='black' variant="h6" component="div" sx={{ flexGrow: 1 }}>
          Language
        </Typography>
        <Button className="custom-button-green">
            <span className="button-link">
              Login</span></Button>
        <Button className="custom-button-yellow">
          <span className='button-link'>Sign Up</span></Button>
      </Toolbar> 
    </AppBar>
  </header>
);

export default Header;
